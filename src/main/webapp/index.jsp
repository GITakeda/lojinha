<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="edu.ifsp.lojinha.modelo.Cliente" %>
<%@ page import="edu.ifsp.lojinha.modelo.Usuario" %>

<%
Cliente cliente = (Cliente)request.getAttribute("cliente");
%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lojinha do Zé</title>
</head>

<body>
	<h1>Lojinha do Zé</h1>
	
<%
	Usuario user = (Usuario) request.getSession().getAttribute("usuario");
	
	if(user != null){
%>
	<p>Olá, <%= user.getNome() %>! <a href="logout">Sair</a></p>
<%
	} else{
%>
	<a href="login">Login</a>
<%
	}
%>
	
	<p>Faça <a href="cadastro.html">aqui</a> o seu cadastro de cliente!
	
<%
	if(user != null){
%>
	<p><a href="listar">Listar clientes</a></p>	
<%
	}
%>
</body>
</html>